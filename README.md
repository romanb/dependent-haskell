# dependent-haskell

Experiments with dependently-typed programming in Haskell based on
examples from the book "Type-Driven Development with Idris".
