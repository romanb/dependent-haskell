-- |
-- State machine for a door with state transitions that may fail.
--
-- Journal entry:
-- https://romanb.gitlab.io/posts/dependent-types-in-haskell-4
--
-- Original Idris code (without the parts for running the state machine):
-- https://github.com/edwinb/TypeDD-Samples/blob/master/Chapter14/DoorJam.idr
--

{-# LANGUAGE
    AllowAmbiguousTypes
  , DataKinds
  , EmptyCase
  , GADTs
  , LambdaCase
  , RankNTypes
  , RebindableSyntax
  , ScopedTypeVariables
  , TypeApplications
  , TypeInType
  , TypeFamilies
  , TypeOperators
#-}

{-# OPTIONS_GHC
    -Wno-name-shadowing
    -Wno-unused-do-bind
#-}

module DoorJam where

import Data.Kind
import Data.Void
import Prelude
import System.CPUTime

------------------------------------------------------------------------------
-- Machinery for defunctionalizing type-level functions.
--
-- This is necessary because type-level functions are not (type-level) values
-- and hence cannot be partially applied.

-- | Carrier for the argument and return kind of a defunctionalized
-- type-level function. Only used at the kind-level!
--
-- /Note/: When 'TyFun' is used at the type-level, the two type parameters must
-- have kind 'Type'. When used at the kind-level, the two kind parameters must
-- also have kind (or "sort"?) 'Type', __but__ 'Type' classifies all kinds on
-- the kind-level (and in particular `Type :: Type`), whereas the kind 'Type'
-- classifies only *types* on the type-level.  Hence 'TyFun' can be used at the
-- kind-level with any kinds as parameters.
--
-- In the particular case of this example, the kinds 'DoorResult' and
-- 'DoorState' are the argument and result kind of the (defunctionalized)
-- version of the type-level function 'CheckDoor'. See 'CheckDoorSym0'.
data TyFun :: Type -> Type -> Type

-- | Type/Kind aliases for function symbols. Only used at the kind-level!
type FunSym a b = TyFun  a b -> Type
type a ~> b     = FunSym a b
infixr 0 ~>

-- | Type-level function application for function symbols,
-- i.e. defunctionalized type-level functions (type families).
type family Apply (f :: k1 ~> k2) (a :: k1) :: k2

------------------------------------------------------------------------------
-- General-purpose singleton machinery.

-- | Poly-kinded data family to generalise over singleton types.
data family Sing :: forall k. k -> Type

-- | An existentially quantified singleton type of a known kind.
data SomeSing k = forall a. SomeSing (Sing (a :: k))

-- | The class of kinds for which there exists a family
-- of singleton types.
class SingKind k where
    fromSing :: Sing (a :: k) -> k
    toSing   :: k -> SomeSing k

data instance Sing :: Void -> Type

instance SingKind Void where
    fromSing = \case
    toSing = \case

------------------------------------------------------------------------------
-- Type-level constant function

-- | Type-level constant function.
type family Const (a :: k1) (b :: k2) :: k1 where
    Const a b = a

-- | Symbols for the defunctionalized 'Const' function,
data ConstSym0 :: k1 ~> k2 ~> k1
data ConstSym1 :: k1 -> k2 ~> k1

-- | Applying the first argument to the symbol for (a -> b -> a)
-- yields the symbol for the remaining function (b -> a).
type instance Apply  ConstSym0 a    = ConstSym1 a
-- | Applying the second argument to the symbol for (b -> a)
-- yields the result a.
type instance Apply (ConstSym1 a) b = Const a b

------------------------------------------------------------------------------
-- Door state machine definition

data DoorState  = DoorOpen | DoorClosed
data DoorResult = DoorJammed | DoorOK
data DoorDo     = DoRing | DoOpen | DoClose | DoInvalid

instance Show DoorState where
    show DoorOpen   = "open"
    show DoorClosed = "closed"

data instance Sing :: () -> Type where
    SUnit :: Sing '()

instance SingKind () where
    fromSing SUnit = ()
    toSing   ()    = SomeSing SUnit

data instance Sing :: DoorResult -> Type where
    SDoorOK     :: Sing 'DoorOK
    SDoorJammed :: Sing 'DoorJammed

instance SingKind DoorResult where
    fromSing SDoorOK       = DoorOK
    fromSing SDoorJammed   = DoorJammed

    toSing    DoorOK       = SomeSing SDoorOK
    toSing    DoorJammed   = SomeSing SDoorJammed

data instance Sing :: DoorState -> Type where
    SDoorOpen   :: Sing 'DoorOpen
    SDoorClosed :: Sing 'DoorClosed

instance SingKind DoorState where
    fromSing SDoorOpen   = DoorOpen
    fromSing SDoorClosed = DoorClosed

    toSing DoorOpen   = SomeSing SDoorOpen
    toSing DoorClosed = SomeSing SDoorClosed

data instance Sing :: DoorDo -> Type where
    SDoRing    :: Sing 'DoRing
    SDoOpen    :: Sing 'DoOpen
    SDoClose   :: Sing 'DoClose
    SDoInvalid :: Sing 'DoInvalid

instance SingKind DoorDo where
    fromSing SDoRing    = DoRing
    fromSing SDoOpen    = DoOpen
    fromSing SDoClose   = DoClose
    fromSing SDoInvalid = DoInvalid

    toSing DoRing    = SomeSing SDoRing
    toSing DoOpen    = SomeSing SDoOpen
    toSing DoClose   = SomeSing SDoClose
    toSing DoInvalid = SomeSing SDoInvalid

-- GHC 8.10 (Visible Dependent Quantification):
-- data Door :: forall (a :: Type) -> DoorState -> (a ~> DoorState) -> Type where
-- data Door (a :: Type) (s :: DoorState) (f :: a ~> DoorState) where
data Door2 :: forall (a :: Type). a -> DoorState -> (a -> DoorState) -> Type where

data Door (a :: Type) :: DoorState -> (a ~> DoorState) -> Type where
    -- State transitions
    Open  :: Door DoorResult 'DoorClosed CheckDoorSym0
    Close :: Door () 'DoorOpen   (ConstSym1 'DoorClosed)
    Ring  :: Door () 'DoorClosed (ConstSym1 'DoorClosed)

    -- Interaction
    GetInput :: Door DoorDo s (ConstSym1 s)
    Display  :: String -> Door () s (ConstSym1 s)

    -- Embedding of pure results and composition of transitions
    -- See Note [The type of Pure]
    Pure :: forall k (a :: k) (f :: k ~> DoorState)
          . Sing a
         -> Door k (Apply f a) f

    Bind :: forall (s :: DoorState) k1 k2
            (f1 :: k1 ~> DoorState)
            (f2 :: k2 ~> DoorState)
          . SingKind k1
         => Door k1 s f1
         -> (forall (a :: k1). Sing a -> Door k2 (Apply f1 a) f2)
         -> Door k2 s f2

-- Note [The type of Pure]
-- ~~~~~~~~~~~~~~~~~~~~~~~
-- Using 'Pure' in a computation allows to freely choose the type-level function
-- @f@ that determines the next state of the state machine, as long as its
-- application on the (type-level) pure value matches the state of the preceding
-- computation. In this way, usage of 'Pure' cannot perform a state transition
-- (which would be undesirable) but allows to hide the fact that a conditional
-- result state from the preceding computation has already been inspected, by
-- reintroducing a type-level function that yields the (already known) state.
-- See 'logOpen' for an example.

-- | Check the result of an attempt at opening the door,
-- returning the next state of the door.
type family CheckDoor (res :: DoorResult) :: DoorState where
    CheckDoor 'DoorOK     = 'DoorOpen
    CheckDoor 'DoorJammed = 'DoorClosed

-- | Symbols for the defunctionalized 'CheckDoor' function.
data CheckDoorSym0 :: DoorResult ~> DoorState

-- | Application of the 0-arity 'CheckDoor' symbol (i.e. the representation
-- of the function as a value), to the only argument.
type instance Apply CheckDoorSym0 a = CheckDoor a

------------------------------------------------------------------------------
-- A sample (static) valid door program.

-- | A static (i.e. non-interactive) door program that starts
-- and ends with the door closed.
doorProg :: Door () 'DoorClosed (ConstSym1 'DoorClosed)
doorProg = do
    Ring
    Ring
    res <- Open
    case res of
        SDoorJammed -> Pure SUnit
        SDoorOK     -> Close
  where
    (>>=) = Bind

    -- This type signature needs to be written out in order to
    -- connect the existential type a with the a in (>>=).
    (>>) :: forall (s :: DoorState) k1 k2
                (f1 :: k1 ~> DoorState)
                (f2 :: k2 ~> DoorState)
              . SingKind k1
             => Door k1 s f1
             -> (forall (a :: k1). Door k2 (Apply f1 a) f2)
             -> Door k2 s f2
    -- (>>) ma mb = Bind ma (\(_ :: Sing a) -> mb @a)
    (>>) ma mb = Bind ma (\(_ :: Sing a) -> mb @a)

-- | A variant of 'Open' that logs the attempt by inspecting
-- the result, but pretending the outcome is not yet known
-- (the type is identical to 'Open').
-- See Note [The type of Pure].
logOpen :: Door DoorResult 'DoorClosed CheckDoorSym0
logOpen = do
    Display "[log] Trying to open the door"
    res <- Open
    case res of
        SDoorJammed -> do
            Display "[log] Jammed"
            Pure SDoorJammed
        SDoorOK -> do
            Display "[log] Success"
            Pure SDoorOK
  where
    (>>=) = Bind

    (>>) :: forall (s :: DoorState) k1 k2
                (f1 :: k1 ~> DoorState)
                (f2 :: k2 ~> DoorState)
              . SingKind k1
             => Door k1 s f1
             -> (forall (a :: k1). Door k2 (Apply f1 a) f2)
             -> Door k2 s f2
    (>>) ma mb = Bind ma (\(_ :: Sing a) -> mb @a)

------------------------------------------------------------------------------

-- | Interpret a 'Door' program as an 'IO' action.
runDoorIO
    :: forall k s f
     . SingKind k
    => Door k s f
    -> IO k
runDoorIO = \case
    -- Execution of state transitions.
    Ring -> return ()
    Open -> do
        t <- read . take 1 . drop 8 . show <$> getCPUTime
        case even (t :: Int) of
            True  -> return DoorOK
            False -> return DoorJammed
    Close -> return ()

    -- Interaction
    Display s -> putStrLn s
    GetInput  -> do
        input <- getLine
        case input of
            "ring"  -> return DoRing
            "open"  -> return DoOpen
            "close" -> return DoClose
            _       -> return DoInvalid

    -- Pure values and sequencing of commands.
    Pure     a -> return (fromSing a)
    Bind cmd f -> do
        a <- runDoorIO cmd
        case toSing a of
            SomeSing sa -> runDoorIO (f sa)

------------------------------------------------------------------------------

-- | Interactive, non-terminating self-satisfied door.
--
-- The use of 'Void' and the kind of @f@ testify that the resulting program has
-- no next state, i.e. does not terminate.
selfSatisfiedDoor :: forall s (f :: Void ~> DoorState). Sing (s :: DoorState) -> Door Void s f
selfSatisfiedDoor s = do
    let x = fromSing s :: DoorState
    Display $ "[The door looks "
        ++ case x of
            DoorOpen   -> "open"
            DoorClosed -> "closed"
        ++ "; Commands: ring, open, close]"
    -- Makes GHC panic (https://ghc.haskell.org/trac/ghc/ticket/15598):
    -- Display $ "The door is " ++ show (fromSing s :: DoorState) ++ "."
    input <- GetInput
    case input of
        SDoRing -> case s of
            SDoorClosed -> do
                Ring
                Display ">> You rang? <<"
                selfSatisfiedDoor s
            _ -> do
                Display ">> Why ring? I'm open! <<"
                selfSatisfiedDoor s
        SDoOpen -> case s of
            SDoorClosed -> do
                res <- Open
                case res of
                    SDoorOK -> do
                        Display ">> Hummmmmmmyummmmmmm ah! <<"
                        selfSatisfiedDoor SDoorOpen
                    SDoorJammed -> do
                        Display ">> I'm stuck! <<"
                        selfSatisfiedDoor s
            _ -> do
                Display ">> I'm already open, get going! <<"
                selfSatisfiedDoor s
        SDoClose -> case s of
            SDoorOpen -> do
                Close
                Display ">> Hummmmmmmyummmmmmm ah! <<"
                selfSatisfiedDoor SDoorClosed
            _ -> do
                Display ">> I'm already closed! <<"
                selfSatisfiedDoor s
        SDoInvalid -> do
            Display ">> Come again? <<"
            selfSatisfiedDoor s
  where
    (>>=) :: forall k1 k2 (s :: DoorState)
             (f1 :: k1 ~> DoorState)
             (f2 :: k2 ~> DoorState)
           . SingKind k1
           => Door k1 s f1
           -> (forall (a :: k1). Sing a -> Door k2 (Apply f1 a) f2)
           -> Door k2 s f2
    (>>=) = Bind

    (>>) :: forall k1 k2 (s :: DoorState)
            (f1 :: k1 ~> DoorState)
            (f2 :: k2 ~> DoorState)
          . SingKind k1
         => Door k1 s f1
         -> (forall (a :: k1). Door k2 (Apply f1 a) f2)
         -> Door k2 s f2
    (>>) ma mb = ma >>= \(_ :: Sing a) -> mb @a

main :: IO Void
main = runDoorIO (selfSatisfiedDoor SDoorClosed)

