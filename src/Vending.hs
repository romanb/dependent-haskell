-- |
-- State machine for a vending machine.
--
-- Journal entry:
-- https://romanb.gitlab.io/posts/dependent-types-in-haskell-3
--
-- Original Idris code (without the parts for running the state machine):
-- https://github.com/edwinb/TypeDD-Samples/blob/master/Chapter13/Vending.idr
--

{-# LANGUAGE
    BangPatterns
  , DataKinds
  , ExistentialQuantification
  , GADTs
  , RebindableSyntax
  , StandaloneDeriving
  , TypeInType
  , TypeFamilies
#-}

{-# OPTIONS_GHC
    -Wno-name-shadowing
#-}

module Vending where

import Data.Kind
import Data.Void
import Text.Read (readMaybe)
import Prelude

import qualified System.IO as IO

------------------------------------------------------------------------------
-- Vending machine

data Nat = Z | S Nat deriving (Eq, Show, Read)

data SNat :: Nat -> Type where
    SZ :: SNat 'Z
    SS :: SNat n -> SNat ('S n)

deriving instance Show (SNat n)

prettySNat :: SNat n -> String
prettySNat = show . toInt (0 :: Int)
  where
    toInt :: Int -> SNat n -> Int
    toInt !i  SZ    = i
    toInt !i (SS n) = toInt (i+1) n

toSomeSNat :: Word -> SomeSNat
toSomeSNat  0 = SomeSNat SZ
toSomeSNat !n =
    case toSomeSNat (n-1) of
        SomeSNat ps -> SomeSNat (SS ps)

sPlus :: SNat a -> SNat b -> SNat (Plus a b)
sPlus  SZ    b = b
sPlus (SS a) b = SS (sPlus a b)

type family Plus (a :: Nat) (b :: Nat) :: Nat where
    Plus  'Z    b = b
    Plus ('S a) b = 'S (Plus a b)

data SomeSNat = forall (n :: Nat). SomeSNat (SNat n)

type VendState = (Nat, Nat)

data SVendState :: VendState -> Type where
    SVendState :: SNat coins -> SNat chocs -> SVendState '(coins, chocs)

data Input = INSERT_COIN | GET_COINS | VEND | REFILL SomeSNat

data Machine :: VendState -> VendState -> Type -> Type where
    InsertCoin :: Machine '(    coins,      chocs)  '(('S coins), chocs) ()
    GetCoins   :: Machine '(('S coins),     chocs)  '(('Z),       chocs) ()
    Vend       :: Machine '(('S coins), ('S chocs)) '(    coins,  chocs) ()
    Refill     :: SNat n -> Machine '(('Z), chocs) '(('Z), Plus n chocs) ()

    Display  :: String -> Machine s s ()
    GetInput :: Machine s s (Maybe Input)

    Pure :: a -> Machine s s a
    Bind :: Machine s1 s2 a -> (a -> Machine s2 s3 b) -> Machine s1 s3 b

machineLoop :: SVendState s -> Machine s s' Void
machineLoop s@(SVendState coins chocs) = do
    Display $ "Coins inserted: " ++ prettySNat coins
    Display $ "Chocolates in stock: " ++ prettySNat chocs
    Display "Options: [vend|insert|change|<number>]"
    input <- GetInput
    case input of
        Just INSERT_COIN -> do
            InsertCoin
            let coins' = SS coins
            Display "Coin inserted."
            machineLoop (SVendState coins' chocs)

        Just GET_COINS -> case coins of
            x@(SS _) -> do
                GetCoins
                Display $ "Ejected " ++ prettySNat x ++ " coin(s)."
                machineLoop (SVendState SZ chocs)
            _ -> do
                Display "No coins to eject."
                machineLoop s

        Just VEND -> case (coins, chocs) of
            (SS coins', SS chocs') -> do
                Vend
                Display "Enjoy!"
                machineLoop (SVendState coins' chocs')
            (SZ, SS _) -> do
                Display "Cannot vend. No coins inserted."
                machineLoop s
            (_, SZ) -> do
                Display "Cannot vend. Out of stock. Please refill."
                machineLoop s

        Just (REFILL n) -> case (coins, n) of
            (SZ, SomeSNat new) -> do
                Refill new
                let chocs' = sPlus new chocs
                Display $ "Added to stock: " ++ prettySNat new
                machineLoop (SVendState SZ chocs')
            _ -> do
                Display "Cannot refill. Coins in machine."
                machineLoop s

        _ -> do
            Display "Invalid input."
            machineLoop s
  where
    (>>=) = Bind
    (>>) ma mb = ma >>= const mb

runMachineIO :: Machine s1 s2 a -> IO a
runMachineIO (Pure a) = pure a
runMachineIO (Bind ma f) = do
    a <- runMachineIO ma
    runMachineIO (f a)
runMachineIO InsertCoin =
    return ()
runMachineIO Vend =
    return ()
runMachineIO GetCoins =
    return ()
runMachineIO (Refill _) =
    return ()
runMachineIO (Display s) =
    IO.putStrLn s
runMachineIO GetInput = do
    l <- IO.getLine
    pure $ case l of
        "insert" -> Just INSERT_COIN
        "vend"   -> Just VEND
        "change" -> Just GET_COINS
        x        -> REFILL . toSomeSNat <$> readMaybe x

main :: IO Void
main = runMachineIO (machineLoop (SVendState SZ SZ) :: Machine '(('Z), ('Z)) '(('Z), ('Z)) Void)

