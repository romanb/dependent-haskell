-- |
-- Provably remove an element from a length-index list (aka vector),
-- given a proof that the element is in the vector.
--
-- Journal entry:
-- https://romanb.gitlab.io/posts/dependent-types-in-haskell-2
--
-- Original Idris code:
-- https://github.com/edwinb/TypeDD-Samples/blob/master/Chapter9/RemoveElem.idr
--

{-# LANGUAGE
    EmptyCase
  , DataKinds
  , ExplicitForAll
  , GADTs
  , LambdaCase
  , StandaloneDeriving
  , TypeFamilies
  , TypeOperators
  , TypeInType
  , FlexibleContexts
  , FlexibleInstances
--  , UndecidableInstances
--  , AllowAmbiguousTypes
#-}

module RemoveElem where

import Data.Kind
import Data.Type.Equality
import Data.Void

-- $setup
-- >>> :set -XDataKinds
-- >>> :set -XTypeOperators
-- >>> :set -XTypeInType

-- | Inductively defined natural numbers.
data Nat = Z | S Nat deriving (Eq, Show)

-- | Kind-indexed data family for singleton types.
data family Sing (a :: k)

-- | Kind class for promoted data types that are used to index
-- a family of singleton types.
class SingKind k where
    fromSing :: Sing (a :: k) -> k

-- | Singleton types for 'Nat's.
data instance Sing (a :: Nat) where
    SZ :: Sing 'Z
    SS :: Sing n -> Sing ('S n)

deriving instance Show (Sing (a :: Nat))

instance SingKind Nat where
    fromSing  SZ    = Z
    fromSing (SS n) = S (fromSing n)

-- | A decidable proposition.
data Dec a = Yes a | No (a -> Void)

-- | Kind class for kinds of singleton types supporting
-- decidable propositional equality.
class DecEq k where
    decEq :: Sing (a :: k) -> Sing (b :: k) -> Dec (a :~: b)

-- | Propositional equality of (singleton-typed) natural numbers.
instance DecEq Nat where
    decEq  SZ     SZ    = Yes Refl
    decEq  SZ    (SS _) = No (\case {})
    decEq (SS _)  SZ    = No (\case {})
    decEq (SS a) (SS b) = case decEq a b of
        No  f    -> No (\case Refl -> f Refl)
        Yes Refl -> Yes Refl

-- | A length-indexed list (aka vector).
data Vect (n :: Nat) (a :: Type) where
    Nil   :: Vect 'Z a
    (:::) :: a -> Vect n a -> Vect ('S n) a

infixr 5 :::

deriving instance Show a => Show (Vect n a)

-- | @Elem a v@ is the proposition that an element of type @a@
-- is contained in a vector of type @v@.
--
-- @Elem@ is poly-kinded in @n@ and @k@.
--
-- Note that this is NOT a family of singleton types, since
-- the are just as many proofs for a proposition that
-- an element is in a vector as there are repeated
-- occurrences of the element, e.g.
--
-- >>> let here  = Here       :: Elem 'Z ('Z '::: 'Z '::: 'Nil)
-- >>> let there = There Here :: Elem 'Z ('Z '::: 'Z '::: 'Nil)
data Elem (x :: k) (xs :: Vect n k) where
    Here  :: Elem x (x '::: xs)
    There :: Elem x xs -> Elem x (y '::: xs)

deriving instance Show (Elem x xs)

-- | Singleton types for 'Vect's.
data instance Sing (xs :: Vect n k) where
    SNil  :: Sing 'Nil
    SCons :: Show (Sing x) => Sing x -> Sing xs -> Sing (x '::: xs)

deriving instance Show (Sing (xs :: Vect n k))

instance SingKind k => SingKind (Vect n k) where
    fromSing  SNil        = Nil
    fromSing (SCons x xs) = fromSing x ::: fromSing xs

-- | Decide whether a value is in a vector.
isElem :: DecEq k => Sing (x :: k) -> Sing (xs :: Vect n k) -> Dec (Elem x xs)
isElem _  SNil          = No (\case {})
isElem x (SCons x' xs') = case decEq x x' of
    Yes Refl   -> Yes Here
    No notHere -> case isElem x xs' of
        Yes there   -> Yes (There there)
        No notThere -> No $ \case
            Here        -> notHere Refl
            There there -> notThere there

-- | Remove an element from a (non-empty, singleton-typed)
-- vector, given a proof that the element is in the vector.
--
-- Note: Returns a demoted version of the given singleton-typed
-- vector with the element removed.
removeElem :: forall (x :: k) (xs :: Vect ('S n) k)
     . SingKind k
    => Elem x xs
    -> Sing xs
    -> Vect n k
removeElem prf (SCons x' xs') = case prf of
    Here        -> fromSing xs'
    There later -> case xs' of
        SNil    -> case later of {}
        SCons{} -> fromSing x' ::: removeElem later xs'

------------------------------------------------------------------------------
-- Variant of removeElem returning a singleton.

-- | Singleton types for 'Elem's in order to define the type-level
-- function 'SRemoveElem'.
data instance Sing (e :: Elem x xs) where
    SHere  :: Sing 'Here
    SThere :: Sing e -> Sing ('There e)

deriving instance Show (Sing (e :: Elem x xs))

-- | Type-level version of 'sRemoveElem'.
type family SRemoveElem (xs :: Vect ('S n) a) (prf :: Elem x xs) :: Vect n a where
    SRemoveElem (x '::: xs)  'Here         = xs
    SRemoveElem (x '::: xs) ('There later) = x '::: SRemoveElem xs later

-- | Fully singleton-typed version of 'removeElem'.
sRemoveElem
    :: Sing (xs :: Vect ('S n) a)
    -> Sing (prf :: Elem x xs)
    -> Sing (SRemoveElem xs prf)
sRemoveElem (SCons x xs) prf = case prf of
    SHere        -> xs
    SThere later -> case xs of
        SNil     -> case later of {}
        SCons{}  -> SCons x (sRemoveElem xs later)

-- Type-level test values.

type V1 = 'Z '::: 'Nil
type V2 = 'S 'Z '::: 'Z '::: 'Nil

type E1 = ('Here :: Elem 'Z V1)
type E2 = ('There 'Here :: Elem 'Z V2)

svect1 :: Sing ('Z '::: 'Nil)
svect1 = SCons SZ SNil

svect2 :: Sing ('S 'Z '::: 'Z '::: 'Nil)
svect2 = SCons (SS SZ) (SCons SZ SNil)

elem1 :: Elem 'Z V1
elem1 = Here

elem2 :: Elem 'Z V2
elem2 = There Here

