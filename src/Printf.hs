-- |
-- Type-safe printf.
--
-- Journal entry:
-- https://romanb.gitlab.io/posts/dependent-types-in-haskell-1
--
-- Original Idris code:
-- https://github.com/edwinb/TypeDD-Samples/blob/master/Chapter6/Printf.idr
--

{-# LANGUAGE
    DataKinds
  , GADTs
  , OverloadedStrings
  , ScopedTypeVariables
  , TypeFamilies
  , TypeOperators
#-}

module Printf where

import Data.Kind

-- $setup
-- >>> :set -XDataKinds

--------------------------------------------------------------
-- Indexed by custom promoted list-like 'Format' type

-- | A list-like data type for representing the order and
-- types of the placeholders in a format string. Only the
-- type-level repersentation obtained through @DataKinds@
-- is used in order to index @SFormat@.
data Format
    = Number Format
    | Str Format
    | End

-- | Note: This is only a singleton type constructor modulo
-- 'SLit', since string literals are irrelevant for the
-- dependent type of 'printf'. For example:
--
-- >>> :t SLit "Hello " (SStr (SLit ", the answer is " (SNumber SEnd)))
-- SLit "Hello " (SStr (SLit ", the answer is " (SNumber SEnd)))
--   :: SFormat ('Str ('Number 'End))
--
-- >>> :t SLit "Hallo " (SStr (SLit ", die Antwort lautet " (SNumber SEnd)))
-- SLit "Hallo " (SStr (SLit ", die Antwort lautet " (SNumber SEnd)))
--   :: SFormat ('Str ('Number 'End))
data SFormat :: Format -> Type where
    SNumber :: SFormat f -> SFormat ('Number f)
    SStr    :: SFormat f -> SFormat ('Str    f)
    SLit    :: String -> SFormat f -> SFormat f
    SEnd    :: SFormat 'End

-- | Turn a type-level value of kind 'Format' into a function type.
--
-- >>> :kind! PrintfType ('Str ('Number 'End))
-- PrintfType ('Str ('Number 'End)) :: *
-- = String -> Int -> String
type family PrintfType (f :: Format) :: Type where
    PrintfType ('Number f) = Int -> PrintfType f
    PrintfType ('Str    f) = String -> PrintfType f
    PrintfType  'End       = String

-- | Construct a string according to a format specification and
-- separate arguments for the placeholders, with the number and
-- types of the arguments determined by the format.
--
-- >>> let f = SLit "Hello " (SStr (SLit ", the answer is " (SNumber SEnd)))
-- >>> :t printf f
-- printf f :: String -> Int -> String
-- >>> printf f "Arthur" 42
-- "Hello Arthur, the answer is 42"
printf :: SFormat f -> PrintfType f
printf = go ""
  where
    go :: String -> SFormat f -> PrintfType f
    go s (SNumber  f) = \(i :: Int) -> go (s ++ show i) f
    go s (SStr     f) = \s' -> go (s ++ s') f
    go s (SLit  s' f) = go (s ++ s') f
    go s SEnd         = s

----------------------------------------------------------------
-- Indexed by type-level list
--
-- This variant needs the @TypeOperators@ extension due to the
-- usage of the promoted type-level list constructor @':@.

-- | Variant of 'SFormat' indexed by a type-level list of types.
data Format' :: [Type] -> Type where
    Number' :: Format' f -> Format' (Int ': f)
    Str'    :: Format' f -> Format' (String ': f)
    Lit'    :: String -> Format' f -> Format' f
    End'    :: Format' '[]

-- | Variant of 'PrintfType'.
type family PrintfType' (tys :: [Type]) :: Type where
    PrintfType' (Int    ': tys) = Int -> PrintfType' tys
    PrintfType' (String ': tys) = String -> PrintfType' tys
    PrintfType' (_      ': tys) = PrintfType' tys
    PrintfType' '[]             = String

-- | Variant of 'printf'.
printf' :: Format' f -> PrintfType' f
printf' = go ""
  where
    go :: String -> Format' f -> PrintfType' f
    go s (Number' f) = \(i :: Int) -> go (s ++ show i) f
    go s (Str'    f) = \s' -> go (s ++ s') f
    go s (Lit' s' f) = go (s ++ s') f
    go s End'        = s

